#!/bin/bash

# html documentation
rm -r ./documentation/

cd ../code
target=`mktemp -d`
mkdir "$target/code"
git archive "$1" | tar xf - -C "$target/code"
cd -
cd "$target/code/doc/"
mkdir -p source/_templates
cat > source/_templates/layout.html << EOF
{% extends "!layout.html" %}
{% block rootrellink %}
    <li><a href="./{{ ((pathto(master_doc))[:-11]+'/..').lstrip('/') }}">Project Homepage</a> &raquo;</li>
    {{ super() }}
{% endblock %}
EOF
make html
cd -
cp -a "$target/code/doc/build/html/" documentation

# description of model
rm model.pdf
cp -a ../model/arbeit.pdf model.pdf

# license
cp -a ../code/LICENSE .

# zip file
cd ..; ./create_zip.sh $1; cd -

# html files
for i in *.htp; do
  htp "$i" "`basename "$i" .htp`.html"
done
